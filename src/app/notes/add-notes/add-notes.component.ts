import { Component, OnInit, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.css']
})
export class AddNotesComponent implements OnInit {

  @Output() noteCreated = new EventEmitter<{heading:string,content:string}>();

  newNoteHeading="";
  newNoteContent="";

  constructor() { }

  onAddNote(){
    this.noteCreated.emit({heading:this.newNoteHeading , content:this.newNoteContent})
  }

  ngOnInit(): void {
  }

}
