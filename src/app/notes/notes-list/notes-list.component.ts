import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Notes } from '../shared/notes.model';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent implements OnInit {
  @Input() newNote:{heading:string , content:string , timestamp:string};

  @Input() index: number;

  @Output() removeNote = new EventEmitter<number>();

  removeNoteFunc() {
    this.removeNote.emit(this.index);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
