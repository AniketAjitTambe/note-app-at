import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Note_App';

  noteList = [{heading:'Hello World!!!',content:'Just checking if note is getting added or not.',timestamp:this.getTimeStamp()},
  {heading:'Second Note',content:'Just checking if note is getting added or not.',timestamp:this.getTimeStamp()}
];

getTimeStamp(){
  let d = new Date()
  return String(d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes())
} 

onNoteCreated(noteData:{heading:string,content:string}){
    this.noteList.push({
      heading:noteData.heading,
      content:noteData.content,
      timestamp:this.getTimeStamp()
    })
  }

  removeNote(data: {index: number}) {
    this.noteList.splice(data.index, 1);
  }
}

